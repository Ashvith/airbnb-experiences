import AirBNBLogo from '../assets/images/airbnb.png'

function Navbar() {
  return (
    <nav>
      <img src={AirBNBLogo} className='nav--logo' />
    </nav>
  )
}

export default Navbar
